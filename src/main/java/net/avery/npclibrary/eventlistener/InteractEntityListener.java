package net.avery.npclibrary.eventlistener;

import net.avery.npclibrary.NPCLibrary;
import net.avery.npclibrary.eventlistener.custom.PlayerInteractNPCEvent;
import net.avery.npclibrary.session.entity.User;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:42
 **/
public class InteractEntityListener implements Listener {

    private final NPCLibrary npcLibrary;

    public InteractEntityListener(NPCLibrary npcLibrary) {
        this.npcLibrary = npcLibrary;
    }

    @EventHandler
    public void onInteractEntity(PlayerInteractEntityEvent event) {
        final Player player = event.getPlayer();
        final Entity entity = event.getRightClicked();
        final User user = npcLibrary.getSession().getUser(player);

        /* if the user does not exists => return */
        if (user == null) return;

        /* loop through the non player characters from a player */
        user.getNonPlayerCharacters().forEach(nonPlayerCharacter -> {
            if (entity.getEntityId() == nonPlayerCharacter.getBukkitEntity().getEntityId()) {
                PlayerInteractNPCEvent npcEvent = new PlayerInteractNPCEvent(player, nonPlayerCharacter);
                Bukkit.getPluginManager().callEvent(npcEvent);
            }
        });
    }
}
