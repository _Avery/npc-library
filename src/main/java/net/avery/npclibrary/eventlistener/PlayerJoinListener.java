package net.avery.npclibrary.eventlistener;

import net.avery.npclibrary.NPCLibrary;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:46
 **/
public class PlayerJoinListener implements Listener {

    private final NPCLibrary npcLibrary;

    public PlayerJoinListener(NPCLibrary npcLibrary) {
        this.npcLibrary = npcLibrary;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();

        /* add a player to the npc library list */
        npcLibrary.getSession().addPlayer(player);
    }
}
