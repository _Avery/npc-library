package net.avery.npclibrary.eventlistener;

import net.avery.npclibrary.NPCLibrary;
import net.avery.npclibrary.session.entity.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:54
 **/
public class PlayerToggleSneakListener implements Listener {

    private final NPCLibrary npcLibrary;

    public PlayerToggleSneakListener(NPCLibrary npcLibrary) {
        this.npcLibrary = npcLibrary;
    }

    @EventHandler
    public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
        final Player player = event.getPlayer();
        final User user = npcLibrary.getSession().getUser(player);

        /* if the user does not exists => return */
        if (user == null) return;

        /* loop through the non player characters from a player */
        user.getNonPlayerCharacters().forEach(nonPlayerCharacter -> {
            double distance = player.getLocation().distance(nonPlayerCharacter.getBukkitEntity().getLocation());

            /* the npc is sneaking with the player if the distance is less than 5 */
            if (distance < 5) {
                nonPlayerCharacter.toggleSneak(event.isSneaking());
            }
        });
    }
}
