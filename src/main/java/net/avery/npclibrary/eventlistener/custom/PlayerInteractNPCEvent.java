package net.avery.npclibrary.eventlistener.custom;

import net.avery.npclibrary.utilities.NonPlayerCharacter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:13
 **/
public class PlayerInteractNPCEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private Player player;
    private NonPlayerCharacter entity;

    public PlayerInteractNPCEvent(Player player, NonPlayerCharacter entity) {
        this.player = player;
        this.entity = entity;
    }

    public Player getPlayer() {
        return player;
    }

    public NonPlayerCharacter getEntity() {
        return entity;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
