package net.avery.npclibrary.eventlistener;

import net.avery.npclibrary.NPCLibrary;
import net.avery.npclibrary.session.entity.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:48
 **/
public class PlayerMoveListener implements Listener {

    private final NPCLibrary npcLibrary;

    public PlayerMoveListener(NPCLibrary npcLibrary) {
        this.npcLibrary = npcLibrary;
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        final User user = npcLibrary.getSession().getUser(player);

        /* if the user does not exists => return */
        if (user == null) return;

        /* loop through the non player characters from a player */
        user.getNonPlayerCharacters().forEach(nonPlayerCharacter -> {
            double distance = player.getLocation().distance(nonPlayerCharacter.getBukkitEntity().getLocation());

            /* the npc´s head follow the player if the distance is less than or equal to 20 */
            if (distance <= 20) {
                if (!nonPlayerCharacter.isVisible) nonPlayerCharacter.respawnEntity();
                nonPlayerCharacter.faceEntity(player.getLocation());
            } else if (distance >= 50) {
                if (nonPlayerCharacter.isVisible) nonPlayerCharacter.despawnEntity(false);
            } else {
                if (!nonPlayerCharacter.isVisible) nonPlayerCharacter.respawnEntity();
            }
        });
    }
}
