package net.avery.npclibrary.eventlistener;

import net.avery.npclibrary.NPCLibrary;
import net.avery.npclibrary.session.entity.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:56
 **/
public class PlayerQuitListener implements Listener {

    private final NPCLibrary npcLibrary;

    public PlayerQuitListener(NPCLibrary npcLibrary) {
        this.npcLibrary = npcLibrary;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        final Player player = event.getPlayer();
        final User user = npcLibrary.getSession().getUser(player);

        /* if the user does not exists => return */
        if (user == null) return;

        /* loop through all non player characters and despawn all npcs */
        user.getNonPlayerCharacters().forEach(nonPlayerCharacter -> nonPlayerCharacter.despawnEntity(true));
    }
}
