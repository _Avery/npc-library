package net.avery.npclibrary.eventlistener;

import net.avery.npclibrary.NPCLibrary;
import net.avery.npclibrary.session.entity.User;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:51
 **/
public class PlayerTeleportListener implements Listener {

    private final NPCLibrary npcLibrary;

    public PlayerTeleportListener(NPCLibrary npcLibrary) {
        this.npcLibrary = npcLibrary;
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        final Player player = event.getPlayer();
        final User user = npcLibrary.getSession().getUser(player);

        /* if the user does not exists => return */
        if (user == null) return;

        /* loop through the non player characters from a player */
        user.getNonPlayerCharacters().forEach(nonPlayerCharacter -> {
            double distance = event.getTo().distance(nonPlayerCharacter.getBukkitEntity().getLocation());

            /* respawn entity if the distance is less than or equal to 50 and the npc is not visible */
            if (distance <= 50) {
                if (!nonPlayerCharacter.isVisible) nonPlayerCharacter.respawnEntity();
            }
        });
    }
}
