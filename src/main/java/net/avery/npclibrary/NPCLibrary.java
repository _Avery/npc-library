package net.avery.npclibrary;

import lombok.Getter;
import net.avery.npclibrary.eventlistener.*;
import net.avery.npclibrary.session.Session;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 08:59
 **/

@Getter
public class NPCLibrary extends JavaPlugin {

    @Getter
    private static NPCLibrary npcLibrary;

    private Session session;

    @Override
    public void onLoad() {
        npcLibrary = this;
    }

    @Override
    public void onEnable() {
        /* fetch all classes */
        fetchClasses();

        /* register all listener */
        registerListener(Bukkit.getPluginManager());

        /* enable message */
        System.out.println("Plugin: " + this.getDescription().getName() + " | Enabled");
    }

    @Override
    public void onDisable() {
        /* disable message */
        System.out.println("Plugin: " + this.getDescription().getName() + " | Disabled");
    }

    /**
     * This method is used for feching all classes
     */
    private void fetchClasses() {
        this.session = new Session(this);
    }

    /**
     * This method registers all the listeners in the plugin
     *
     * @param pluginManager the bukkit plugin manager
     */
    private void registerListener(final PluginManager pluginManager) {
        pluginManager.registerEvents(new InteractEntityListener(this), this);
        pluginManager.registerEvents(new PlayerJoinListener(this), this);
        pluginManager.registerEvents(new PlayerMoveListener(this), this);
        pluginManager.registerEvents(new PlayerQuitListener(this), this);
        pluginManager.registerEvents(new PlayerTeleportListener(this), this);
        pluginManager.registerEvents(new PlayerToggleSneakListener(this), this);
    }
}
