package net.avery.npclibrary.utilities;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:15
 **/
public class SkinProfile {

    private ProfileType profileType;
    private String profileName;

    /* ProfileType SKIN_VALUES */
    private String value;
    private String signature;

    /* ProfileType PLAYER_SKIN */
    private Player player;

    /* ProfileType DOWNLOAD */
    private UUID uuid;

    private SkinProfile(ProfileType profileType, String profileName, String value, String signature) {
        this.profileType = profileType;
        this.profileName = profileName;
        this.value = value;
        this.signature = signature;
    }

    private SkinProfile(ProfileType profileType, String profileName, Player player) {
        this.profileType = profileType;
        this.profileName = profileName;
        this.player = player;
    }

    private SkinProfile(ProfileType profileType, String profileName, UUID uuid) {
        this.profileType = profileType;
        this.profileName = profileName;
        this.uuid = uuid;
    }

    /**
     * The loading cache
     */
    private final LoadingCache<UUID, GameProfile> properties = CacheBuilder.newBuilder()
            .expireAfterAccess(5, TimeUnit.MINUTES)
            .build(new CacheLoader<UUID, GameProfile>() {

                @Override
                public GameProfile load(UUID uuid) throws Exception {
                    return MinecraftServer.getServer().aD().fillProfileProperties(new GameProfile(uuid, null), true);
                }
            });

    /**
     * The method transform all types to a gameprofile
     *
     * @return @{@link GameProfile}
     */
    public GameProfile transformToGameProfile() {
        profileName = ChatColor.translateAlternateColorCodes('&', profileName);

        switch (profileType) {
            case DOWNLOAD:
                GameProfile skinProfile = properties.getUnchecked(uuid);
                GameProfile profile = new GameProfile(UUID.randomUUID(), profileName);
                profile.getProperties().removeAll("textures");
                profile.getProperties().putAll("textures", skinProfile.getProperties().get("textures"));
                return profile;
            case PLAYER_SKIN:
                GameProfile playerProfile = ((CraftPlayer) player).getProfile();
                GameProfile profileSkin = new GameProfile(UUID.randomUUID(), profileName);
                profileSkin.getProperties().removeAll("textures");
                profileSkin.getProperties().putAll("textures", playerProfile.getProperties().get("textures"));
                return profileSkin;
            case SKIN_VALUES:
                GameProfile userProfile = new GameProfile(UUID.randomUUID(), profileName);
                userProfile.getProperties().removeAll("textures");
                userProfile.getProperties().put("textures", new Property("textures", value, signature));
                return userProfile;
            default:
                return new GameProfile(UUID.randomUUID(), profileName);
        }

    }

    /**
     * This method creates a new skin profile with a value and signature
     *
     * @param profileType the @{@link ProfileType} (explained above)
     * @param profileName the name for the skin profile
     * @param value       the skin value
     * @param signature   the skin signature
     * @return @{@link SkinProfile}
     */
    public static SkinProfile createNewProfile(ProfileType profileType, String profileName, String value, String signature) {
        return new SkinProfile(profileType, profileName, value, signature);
    }

    /**
     * This method creates a new skin profile with the given player
     *
     * @param profileType the @{@link ProfileType} (explained above)
     * @param profileName the name for the skin profile
     * @param player      the player for the skin profile
     * @return @{@link SkinProfile}
     */
    public static SkinProfile createNewProfile(ProfileType profileType, String profileName, Player player) {
        return new SkinProfile(profileType, profileName, player);
    }

    /**
     * This method creates a new skin profile with an given uuid
     *
     * @param profileType the @{@link ProfileType} (explained above)
     * @param profileName the name for the skin profile
     * @param uuid        the uuid for the skin profile
     * @return @{@link SkinProfile}
     */
    public static SkinProfile createNewProfile(ProfileType profileType, String profileName, UUID uuid) {
        return new SkinProfile(profileType, profileName, uuid);
    }
}
