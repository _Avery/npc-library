package net.avery.npclibrary.utilities;

import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:15
 **/
public class Reflections {

    /**
     * Method for sending packet to a specific player
     *
     * @param player the player where the packet should be sent
     * @param packet the packet you want to send
     */
    public static void sendPacket(Player player, Packet packet) {
        if (player != null && player.isOnline()) {
            PlayerConnection playerConnection = ((CraftPlayer) player).getHandle().playerConnection;
            playerConnection.sendPacket(packet);
        }
    }

    /**
     * Method for sending packet to all players
     *
     * @param packet the packet you want to send
     */
    public static void sendPacket(Packet packet) {
        Bukkit.getOnlinePlayers().forEach(player -> sendPacket(player, packet));
    }
}
