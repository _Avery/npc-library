package net.avery.npclibrary.utilities;

import net.avery.npclibrary.NPCLibrary;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.function.Consumer;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:15
 **/
public class EntityFactory {

    /**
     * This method is for building an entity with the given parameter
     *
     * @param player      the player for whom the npc should spawn
     * @param location    the location where the npc should be spawned
     * @param skinProfile the skinProfile for the npc
     * @param name        the name of the npc
     * @param consumer    a consumer with the given @{@link NonPlayerCharacter} (async)
     */
    public static void buildEntity(Player player, Location location, SkinProfile skinProfile, String name, Consumer<NonPlayerCharacter> consumer) {
        NonPlayerCharacter nonPlayerCharacter = NonPlayerCharacter.createNewNPC(NPCLibrary.getNpcLibrary(), player, location, skinProfile, name);
        consumer.accept(nonPlayerCharacter);
    }
}
