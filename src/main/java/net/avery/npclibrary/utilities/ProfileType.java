package net.avery.npclibrary.utilities;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:15
 **/
public enum ProfileType {

    /**
     * The profile types used in the class @{@link SkinProfile}
     */
    PLAYER_SKIN, SKIN_VALUES, DOWNLOAD

}
