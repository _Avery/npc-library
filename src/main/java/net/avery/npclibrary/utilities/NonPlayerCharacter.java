package net.avery.npclibrary.utilities;

import net.avery.npclibrary.NPCLibrary;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Player;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:15
 **/
public class NonPlayerCharacter extends EntityPlayer {

    private final NPCLibrary npcLibrary;

    private Player player;
    private Location location;
    private String name;

    private boolean rotationMatrix;
    public boolean isVisible;

    private NonPlayerCharacter
            (NPCLibrary npcLibrary, Player player, Location location, SkinProfile skinProfile, String name) {
        super(getMinecraftServer(), getWorldServer(location), skinProfile.transformToGameProfile(), new PlayerInteractManager(getWorldServer(location)));

        this.npcLibrary = npcLibrary;

        this.player = player;
        this.location = location;
        this.name = name;
        this.rotationMatrix = false;
        this.isVisible = true;

        world.addEntity(this);
        spawnEntity();

        npcLibrary.getSession().getUser(player).addNPC(this);
    }

    /**
     * This method control if the npc´s head should follow you
     *
     * @param status a boolean, if set to true: npc´s head follow you
     */
    public void setRotationMatrix(boolean status) {
        rotationMatrix = status;
    }

    /**
     * This method allows you to despawn the npc
     *
     * @param despawn a boolean, if set to true: the npc will be despawned
     */
    public void despawnEntity(boolean despawn) {
        PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy(this.getBukkitEntity().getEntityId());
        Reflections.sendPacket(player, destroy);

        if (despawn) world.removeEntity(this);

        this.isVisible = false;
    }

    /**
     * Respawn an npc
     */
    public void respawnEntity() {
        spawnEntity();

        this.isVisible = true;
    }

    /**
     * This method return the minecraft server
     *
     * @return @{@link MinecraftServer}
     */
    private static MinecraftServer getMinecraftServer() {
        return ((CraftServer) Bukkit.getServer()).getServer();
    }

    /**
     * This method return the world server by the given location
     *
     * @param location the location to check
     * @return @{@link WorldServer}
     */
    private static WorldServer getWorldServer(Location location) {
        return ((CraftWorld) location.getWorld()).getHandle();
    }

    /**
     * This method spawns the entity and send all important packets
     */
    private void spawnEntity() {

        /* handle player connection and interact manager */
        playerConnection = new PlayerConnection(getMinecraftServer(), new NetworkManager(EnumProtocolDirection.SERVERBOUND), this);
        playerInteractManager.a((WorldServer) world);
        playerInteractManager.b(WorldSettings.EnumGamemode.CREATIVE);

        /* add entity to world and set location */
        setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());

        /* handle skin overlay with metadata */
        getDataWatcher().watch(10, (byte) 127);

        /* handle spawning with packets */
        PacketPlayOutEntityMetadata metadata = new PacketPlayOutEntityMetadata(getId(), getDataWatcher(), true);
        PacketPlayOutPlayerInfo playerAddInfo = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, this);
        PacketPlayOutNamedEntitySpawn entitySpawn = new PacketPlayOutNamedEntitySpawn(this);
        PacketPlayOutPlayerInfo playerRemoveInfo = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, this);
        PacketPlayOutEntityTeleport teleport = new PacketPlayOutEntityTeleport(this);
        PacketPlayOutEntityHeadRotation headRotation = new PacketPlayOutEntityHeadRotation(this, (byte) (location.getYaw() * 256F / 360F));

        /* sending packets */
        Reflections.sendPacket(player, metadata);
        Reflections.sendPacket(player, playerAddInfo);
        Reflections.sendPacket(player, entitySpawn);
        Bukkit.getScheduler().runTaskLater(npcLibrary, () -> {
            Reflections.sendPacket(player, playerRemoveInfo);
            Reflections.sendPacket(player, teleport);
            Reflections.sendPacket(player, headRotation);
        }, 10);
    }

    /**
     * Method to control if the player should sneak
     *
     * @param sneak boolean if this is true, the npc also sneak
     */
    public void toggleSneak(boolean sneak) {
        setSneaking(sneak);
    }

    /**
     * This method controls the face (head rotation) for the npc
     *
     * @param to the location where the npc should look
     */
    public void faceEntity(Location to) {
        if (!rotationMatrix) return;
        Entity handle = getHandle(getBukkitEntity());
        Location fromLocation = location;
        double xDiff, yDiff, zDiff;

        xDiff = to.getX() - fromLocation.getX();
        yDiff = to.getY() - fromLocation.getY();
        zDiff = to.getZ() - fromLocation.getZ();

        double distanceXZ = Math.sqrt(xDiff * xDiff + zDiff * zDiff);
        double distanceY = Math.sqrt(distanceXZ * distanceXZ + yDiff * yDiff);

        double yaw = Math.toDegrees(Math.acos(xDiff / distanceXZ));
        double pitch = Math.toDegrees(Math.acos(yDiff / distanceY)) - 90;
        if (zDiff < 0.0) {
            yaw += Math.abs(180 - yaw) * 2;
        }
        yaw = yaw - 90;

        setLocation(location.getX(), location.getY(), location.getZ(), (float) yaw, (float) pitch);
        handle.pitch = (float) pitch;

        PacketPlayOutEntityTeleport entityTeleport = new PacketPlayOutEntityTeleport(this);
        PacketPlayOutEntityHeadRotation headRotation = new PacketPlayOutEntityHeadRotation(this, (byte) (yaw * 256F / 360F));

        Reflections.sendPacket(player, entityTeleport);
        Reflections.sendPacket(player, headRotation);
    }

    /**
     * This method return the entity handle if the entity is an instanceof CraftEntity
     *
     * @param entity the given entity for check
     * @return @{@link Entity}
     */
    private Entity getHandle(org.bukkit.entity.Entity entity) {
        if (!(entity instanceof CraftEntity))
            return null;
        return ((CraftEntity) entity).getHandle();
    }

    /**
     * This method creates an new NPC
     *
     * @param npcLibrary  the @{@link NPCLibrary} instance
     * @param player      the player for whom the npc should spawn
     * @param location    the location where the npc should be spawned
     * @param skinProfile the skinProfile for the npc
     * @param name        the name of the npc
     * @return @{@link NonPlayerCharacter}
     */
    public static NonPlayerCharacter createNewNPC(NPCLibrary npcLibrary, Player player, Location location, SkinProfile skinProfile, String name) {
        return new NonPlayerCharacter(npcLibrary, player, location, skinProfile, name);
    }
}