package net.avery.npclibrary.session;

import com.google.common.collect.Maps;
import net.avery.npclibrary.NPCLibrary;
import net.avery.npclibrary.session.entity.User;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:00
 **/
public class Session {

    private final NPCLibrary npcLibrary;

    private HashMap<UUID, User> users;

    public Session(NPCLibrary npcLibrary) {
        this.npcLibrary = npcLibrary;
        this.users = Maps.newHashMap();
    }

    /**
     * Add a player to the users map
     *
     * @param player the player will be putted into the map
     */
    public void addPlayer(Player player) {
        users.put(player.getUniqueId(), new User(player));
    }

    /**
     * Remove a user from the users map
     *
     * @param player the player to remove
     */
    public void removeUser(Player player) {
        users.remove(player.getUniqueId());
    }

    /**
     * Get a user from the users map
     *
     * @param player the given player
     * @return @{@link User}
     */
    public User getUser(Player player) {
        return users.get(player.getUniqueId());
    }
}
