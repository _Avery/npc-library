package net.avery.npclibrary.session.entity;

import com.google.common.collect.Lists;
import lombok.Getter;
import net.avery.npclibrary.utilities.NonPlayerCharacter;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Copyright ©_Avery 2016-2019
 * Erstellt von _Avery | Lukas am 13.09.2019 um 09:08
 **/

@Getter
public class User {

    private Player player;

    private List<NonPlayerCharacter> nonPlayerCharacters;

    public User(Player player) {
        this.player = player;
        this.nonPlayerCharacters = Lists.newArrayList();
    }

    /**
     * This method allows to add an nonPlayerCharacter to the list of the player
     *
     * @param nonPlayerCharacter the given @{@link NonPlayerCharacter} add into the list
     */
    public void addNPC(NonPlayerCharacter nonPlayerCharacter) {
        nonPlayerCharacters.add(nonPlayerCharacter);
    }
}
